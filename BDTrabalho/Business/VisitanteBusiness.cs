﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Business
{
    class VisitanteBusiness
    {
        public void InserirVisitante(Model.VisitanteModel visitante)
        {
            if (visitante.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.VisitanteDatabase visitaDatabase = new Database.VisitanteDatabase();
            visitaDatabase.InserirVisitante(visitante);
        }

        public List<Model.VisitanteModel> FiltrarPorNome(string nome)
        {
            Database.VisitanteDatabase visitanteDatabase = new Database.VisitanteDatabase();
            List<Model.VisitanteModel> lista = visitanteDatabase.FiltrarPorNome(nome);

            return lista;
        }
    }
}
