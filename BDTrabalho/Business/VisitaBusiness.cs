﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Business
{
    class VisitaBusiness
    {
        public List<Model.VisitanteModel> ListarVisitante()
        {
            Database.VisitanteDatabase db = new Database.VisitanteDatabase();
            List<Model.VisitanteModel> visitantes = db.ListarVisitante();
            return visitantes;
        }

        public List<Model.MoradorModel> ListarMoradores()
        {
            Database.MoradorDatabase db = new Database.MoradorDatabase();
            List<Model.MoradorModel> moradores = db.ListarMoradores();

            return moradores;
        }

        public void MarcarVisita(Model.VisitaModel model)
        {
            if(model.conhecido == true)
            {
                model.tipodevisita = "Conhecido";
            }
            else
            {
                model.tipodevisita = "Desconhecido";
            }

            Database.VisitaDatabase marcarvisitaDatabase = new Database.VisitaDatabase();
            marcarvisitaDatabase.MarcarVisita(model);
        }


        public List<Model.ConsultaVisitanteView> ListarVisitas(string visitante, DateTime data)
        {
            Database.VisitaDatabase marcarvisitaDatabase = new Database.VisitaDatabase();
            List<Model.ConsultaVisitanteView> lista = marcarvisitaDatabase.ListarVisitas(visitante, data);

            return lista;
        }

        public void Alterar(Model.VisitaModel model)
        {
            if (model.conhecido == true)
            {
                model.tipodevisita = "Conhecido";
            }
            else
            {
                model.tipodevisita = "Desconhecido";
            }

            Database.VisitaDatabase marcarvisitaDatabase = new Database.VisitaDatabase();
            marcarvisitaDatabase.Alterar(model);
        }

        public List<Model.ConsultaVisitanteView> ListarVisitasPorVisitante(string visitante)
        {
            Database.VisitaDatabase marcarvisitaDatabase = new Database.VisitaDatabase();
            List<Model.ConsultaVisitanteView> lista = marcarvisitaDatabase.ListarVisitasPorVisitante(visitante);

            return lista;
        }

        public void Deletar(int id)
        {
            Database.VisitaDatabase visitaDatabase = new Database.VisitaDatabase();
            visitaDatabase.Deletar(id);
        }
    }
}
