﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Business
{
    class MoradorBusiness
    {
        public void InserirMorador(Model.MoradorModel morador)
        {
            if (morador.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (morador.NdeContato == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório");
            }

            if (morador.Bloco == string.Empty)
            {
                throw new ArgumentException("Bloco é obrigatório");
            }

            Database.MoradorDatabase moradorDatabase = new Database.MoradorDatabase();
            moradorDatabase.InserirMorador(morador);
        }

        public List<Model.MoradorModel> FiltrarPorNome(string nome)
        {
            Database.MoradorDatabase moradorDatabase = new Database.MoradorDatabase();
            List<Model.MoradorModel> lista = moradorDatabase.FiltrarPorNome(nome);

            return lista;
        }
    }
}
