﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Business
{
    class LoginBusiness
    {
        public Model.LoginModel Login(string usuario, string senha)
        {
            Database.LoginDatabase loginDB = new Database.LoginDatabase();
            Model.LoginModel login = loginDB.Login(usuario, senha);

            return login;
        }

        public void Cadastrar(string usuario, string senha)
        {
            if (usuario == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");
            }

            Database.LoginDatabase loginDatabase = new Database.LoginDatabase();
            loginDatabase.Cadastrar(usuario, senha);
        }
    }
}
