﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmConsultarVisitante : Form
    {
        public frmConsultarVisitante()
        {
            InitializeComponent();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.Consultar();
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void Consultar()
        {
            try
            {
                string nome = txtNome.Text;

                Business.VisitanteBusiness VisitaBusiness = new Business.VisitanteBusiness();
                List<Model.VisitanteModel> lista = VisitaBusiness.FiltrarPorNome(nome);

                dgvVisitante.AutoGenerateColumns = false;
                dgvVisitante.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }
    }
}
