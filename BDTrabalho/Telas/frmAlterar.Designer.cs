﻿namespace BDTrabalho.Telas
{
    partial class frmAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFechar = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvVisita = new System.Windows.Forms.DataGridView();
            this.Visitante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Morador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdAdultos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdCriancas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TpTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TpVisita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraChegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtHrdeChegada = new System.Windows.Forms.MaskedTextBox();
            this.btnOk = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.rdnDesconhecido = new System.Windows.Forms.RadioButton();
            this.rdnConhecido = new System.Windows.Forms.RadioButton();
            this.cboTipodeTransporte = new System.Windows.Forms.ComboBox();
            this.nudQtddeCriancas = new System.Windows.Forms.NumericUpDown();
            this.nudQtddeAdultos = new System.Windows.Forms.NumericUpDown();
            this.cboVisitante = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtddeCriancas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtddeAdultos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Black;
            this.lblFechar.Location = new System.Drawing.Point(567, -2);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(19, 21);
            this.lblFechar.TabIndex = 5;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::BDTrabalho.Properties.Resources.background_image;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.dgvVisita);
            this.groupBox1.Controls.Add(this.txtHrdeChegada);
            this.groupBox1.Controls.Add(this.btnOk);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtpData);
            this.groupBox1.Controls.Add(this.rdnDesconhecido);
            this.groupBox1.Controls.Add(this.rdnConhecido);
            this.groupBox1.Controls.Add(this.cboTipodeTransporte);
            this.groupBox1.Controls.Add(this.nudQtddeCriancas);
            this.groupBox1.Controls.Add(this.nudQtddeAdultos);
            this.groupBox1.Controls.Add(this.cboVisitante);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(563, 601);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alterar";
            // 
            // dgvVisita
            // 
            this.dgvVisita.AllowUserToAddRows = false;
            this.dgvVisita.AllowUserToDeleteRows = false;
            this.dgvVisita.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvVisita.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisita.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Visitante,
            this.Morador,
            this.QtdAdultos,
            this.QtdCriancas,
            this.TpTransporte,
            this.TpVisita,
            this.HoraChegada,
            this.Data});
            this.dgvVisita.Location = new System.Drawing.Point(10, 81);
            this.dgvVisita.Name = "dgvVisita";
            this.dgvVisita.ReadOnly = true;
            this.dgvVisita.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVisita.Size = new System.Drawing.Size(543, 216);
            this.dgvVisita.TabIndex = 27;
            this.dgvVisita.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVisita_CellClick);
            // 
            // Visitante
            // 
            this.Visitante.DataPropertyName = "Visitante";
            this.Visitante.HeaderText = "Visitante";
            this.Visitante.Name = "Visitante";
            this.Visitante.ReadOnly = true;
            // 
            // Morador
            // 
            this.Morador.DataPropertyName = "Morador";
            this.Morador.HeaderText = "Morador";
            this.Morador.Name = "Morador";
            this.Morador.ReadOnly = true;
            // 
            // QtdAdultos
            // 
            this.QtdAdultos.DataPropertyName = "QtdAdultos";
            this.QtdAdultos.HeaderText = "Adultos";
            this.QtdAdultos.Name = "QtdAdultos";
            this.QtdAdultos.ReadOnly = true;
            // 
            // QtdCriancas
            // 
            this.QtdCriancas.DataPropertyName = "QtdCriancas";
            this.QtdCriancas.HeaderText = "Crianças";
            this.QtdCriancas.Name = "QtdCriancas";
            this.QtdCriancas.ReadOnly = true;
            // 
            // TpTransporte
            // 
            this.TpTransporte.DataPropertyName = "TpTransporte";
            this.TpTransporte.HeaderText = "Tipo de Transporte";
            this.TpTransporte.Name = "TpTransporte";
            this.TpTransporte.ReadOnly = true;
            // 
            // TpVisita
            // 
            this.TpVisita.DataPropertyName = "TpVisita";
            this.TpVisita.HeaderText = "Tipo de Visita";
            this.TpVisita.Name = "TpVisita";
            this.TpVisita.ReadOnly = true;
            // 
            // HoraChegada
            // 
            this.HoraChegada.DataPropertyName = "HoraChegada";
            this.HoraChegada.HeaderText = "Hora de Chegada";
            this.HoraChegada.Name = "HoraChegada";
            this.HoraChegada.ReadOnly = true;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // txtHrdeChegada
            // 
            this.txtHrdeChegada.Location = new System.Drawing.Point(146, 459);
            this.txtHrdeChegada.Mask = "##:##";
            this.txtHrdeChegada.Name = "txtHrdeChegada";
            this.txtHrdeChegada.Size = new System.Drawing.Size(153, 29);
            this.txtHrdeChegada.TabIndex = 26;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.Transparent;
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.Image = global::BDTrabalho.Properties.Resources.add;
            this.btnOk.Location = new System.Drawing.Point(497, 539);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(56, 51);
            this.btnOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnOk.TabIndex = 25;
            this.btnOk.TabStop = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(6, 500);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 21);
            this.label8.TabIndex = 24;
            this.label8.Text = "Data:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(6, 462);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 21);
            this.label7.TabIndex = 23;
            this.label7.Text = "Hora de Chegada:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(6, 420);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 21);
            this.label6.TabIndex = 22;
            this.label6.Text = "Tipo de Visita:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(6, 392);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 21);
            this.label5.TabIndex = 21;
            this.label5.Text = "Tipo de Transporte:";
            // 
            // dtpData
            // 
            this.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpData.Location = new System.Drawing.Point(146, 500);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(153, 29);
            this.dtpData.TabIndex = 19;
            // 
            // rdnDesconhecido
            // 
            this.rdnDesconhecido.AutoSize = true;
            this.rdnDesconhecido.BackColor = System.Drawing.Color.Transparent;
            this.rdnDesconhecido.Location = new System.Drawing.Point(130, 416);
            this.rdnDesconhecido.Name = "rdnDesconhecido";
            this.rdnDesconhecido.Size = new System.Drawing.Size(125, 25);
            this.rdnDesconhecido.TabIndex = 17;
            this.rdnDesconhecido.TabStop = true;
            this.rdnDesconhecido.Text = "Desconhecido";
            this.rdnDesconhecido.UseVisualStyleBackColor = false;
            // 
            // rdnConhecido
            // 
            this.rdnConhecido.AutoSize = true;
            this.rdnConhecido.BackColor = System.Drawing.Color.Transparent;
            this.rdnConhecido.Location = new System.Drawing.Point(266, 420);
            this.rdnConhecido.Name = "rdnConhecido";
            this.rdnConhecido.Size = new System.Drawing.Size(102, 25);
            this.rdnConhecido.TabIndex = 16;
            this.rdnConhecido.TabStop = true;
            this.rdnConhecido.Text = "Conhecido";
            this.rdnConhecido.UseVisualStyleBackColor = false;
            // 
            // cboTipodeTransporte
            // 
            this.cboTipodeTransporte.FormattingEnabled = true;
            this.cboTipodeTransporte.Items.AddRange(new object[] {
            "Ônibus",
            "Carro Particular",
            "Uber/Taxi",
            "Moto",
            "Bicicleta",
            "A pé"});
            this.cboTipodeTransporte.Location = new System.Drawing.Point(163, 389);
            this.cboTipodeTransporte.Name = "cboTipodeTransporte";
            this.cboTipodeTransporte.Size = new System.Drawing.Size(136, 29);
            this.cboTipodeTransporte.TabIndex = 15;
            // 
            // nudQtddeCriancas
            // 
            this.nudQtddeCriancas.Location = new System.Drawing.Point(163, 349);
            this.nudQtddeCriancas.Name = "nudQtddeCriancas";
            this.nudQtddeCriancas.Size = new System.Drawing.Size(136, 29);
            this.nudQtddeCriancas.TabIndex = 14;
            // 
            // nudQtddeAdultos
            // 
            this.nudQtddeAdultos.Location = new System.Drawing.Point(163, 314);
            this.nudQtddeAdultos.Name = "nudQtddeAdultos";
            this.nudQtddeAdultos.Size = new System.Drawing.Size(136, 29);
            this.nudQtddeAdultos.TabIndex = 13;
            // 
            // cboVisitante
            // 
            this.cboVisitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVisitante.FormattingEnabled = true;
            this.cboVisitante.Location = new System.Drawing.Point(266, 46);
            this.cboVisitante.Name = "cboVisitante";
            this.cboVisitante.Size = new System.Drawing.Size(287, 29);
            this.cboVisitante.TabIndex = 11;
            this.cboVisitante.SelectedIndexChanged += new System.EventHandler(this.cboVisitante_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(6, 357);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "Qtd de Crianças:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(6, 322);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "Qtd de Adultos:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Visitante:";
            // 
            // frmAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(584, 622);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmAlterar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtddeCriancas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtddeAdultos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox txtHrdeChegada;
        private System.Windows.Forms.PictureBox btnOk;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.RadioButton rdnDesconhecido;
        private System.Windows.Forms.RadioButton rdnConhecido;
        private System.Windows.Forms.ComboBox cboTipodeTransporte;
        private System.Windows.Forms.NumericUpDown nudQtddeCriancas;
        private System.Windows.Forms.NumericUpDown nudQtddeAdultos;
        private System.Windows.Forms.ComboBox cboVisitante;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvVisita;
        private System.Windows.Forms.DataGridViewTextBoxColumn Visitante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Morador;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdAdultos;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdCriancas;
        private System.Windows.Forms.DataGridViewTextBoxColumn TpTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn TpVisita;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraChegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
    }
}