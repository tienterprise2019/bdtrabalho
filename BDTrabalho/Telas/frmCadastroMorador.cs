﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmCadastroMorador : Form
    {
        public frmCadastroMorador()
        {
            InitializeComponent();
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        private void Cadastrar()
        {
            try
            {
                Model.MoradorModel morador = new Model.MoradorModel();
                morador.Nome = txtNome.Text;
                morador.NdeContato = txtNdeContato.Text;
                morador.Bloco = txtBloco.Text;
                morador.NdoAp = Convert.ToInt32(txtNdoAp.Text);

                Business.MoradorBusiness moradorBusiness = new Business.MoradorBusiness();
                moradorBusiness.InserirMorador(morador);

                MessageBox.Show("Morador inserido com sucesso.", "Morador", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
