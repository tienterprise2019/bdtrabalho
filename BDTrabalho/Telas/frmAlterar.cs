﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();

            this.CarregarVisitantes();
        }
        private void CarregarVisitantes()
        {
            Business.VisitaBusiness marcarvisitabusiness = new Business.VisitaBusiness();
            List<Model.VisitanteModel> visitantes = marcarvisitabusiness.ListarVisitante();

            cboVisitante.DisplayMember = nameof(Model.VisitanteModel.Nome);
            cboVisitante.DataSource = visitantes;
        }
        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Model.VisitanteModel visitante = cboVisitante.SelectedItem as Model.VisitanteModel;
                Model.ConsultaVisitanteView m = dgvVisita.CurrentRow.DataBoundItem as Model.ConsultaVisitanteView;

                Model.VisitaModel model = new Model.VisitaModel();
                model.Id = m.IdVisita;
                model.qtddeadultos = Convert.ToInt32(nudQtddeAdultos.Value);
                model.qtddecriancas = Convert.ToInt32(nudQtddeCriancas.Value);
                model.tipodetransporte = cboTipodeTransporte.Text;
                model.conhecido = rdnConhecido.Checked;
                model.desconhecido = rdnDesconhecido.Checked;
                model.horadechegada = TimeSpan.Parse(txtHrdeChegada.Text);
                model.data = dtpData.Value;

                Business.VisitaBusiness visitaBusiness = new Business.VisitaBusiness();
                visitaBusiness.Alterar(model);

                MessageBox.Show("Visita alterada com sucesso.", "Alterar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void cboVisitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            string visitante = cboVisitante.Text;

            Business.VisitaBusiness b = new Business.VisitaBusiness();
            List<Model.ConsultaVisitanteView> lista = b.ListarVisitasPorVisitante(visitante);

            dgvVisita.AutoGenerateColumns = false;
            dgvVisita.DataSource = lista;
        }

        private void dgvVisita_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Model.ConsultaVisitanteView m = dgvVisita.CurrentRow.DataBoundItem as Model.ConsultaVisitanteView;

            nudQtddeAdultos.Value = m.QtdAdultos;
            nudQtddeCriancas.Value = m.QtdCriancas;
            cboTipodeTransporte.Text = m.TpTransporte;
            txtHrdeChegada.Text = m.HoraChegada.ToString("hh\\:mm");
            dtpData.Value = m.Data;
            if(m.TpVisita == "Conhecido")
            {
                rdnConhecido.Checked = true;
                rdnDesconhecido.Checked = false;
            }
            else
            {
                rdnDesconhecido.Checked = true;
                rdnConhecido.Checked = false;
            }
        }
    }
}
