﻿namespace BDTrabalho.Telas
{
    partial class frmConsultarVisita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFechar = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.dgvVisita = new System.Windows.Forms.DataGridView();
            this.Visitante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Morador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdAdultos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdCriancas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TpTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TpVisita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraChegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNomedoVisitante = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisita)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblFechar.Location = new System.Drawing.Point(564, 0);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(19, 21);
            this.lblFechar.TabIndex = 4;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.LblFechar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::BDTrabalho.Properties.Resources.background_image;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpData);
            this.groupBox1.Controls.Add(this.dgvVisita);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtNomedoVisitante);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(555, 344);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consultar Visita";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(6, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 21);
            this.label1.TabIndex = 12;
            this.label1.Text = "Data  da Visita:";
            // 
            // dtpData
            // 
            this.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpData.Location = new System.Drawing.Point(262, 82);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(287, 29);
            this.dtpData.TabIndex = 11;
            this.dtpData.ValueChanged += new System.EventHandler(this.dtpData_ValueChanged);
            // 
            // dgvVisita
            // 
            this.dgvVisita.AllowUserToAddRows = false;
            this.dgvVisita.AllowUserToDeleteRows = false;
            this.dgvVisita.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvVisita.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisita.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Visitante,
            this.Morador,
            this.QtdAdultos,
            this.QtdCriancas,
            this.TpTransporte,
            this.TpVisita,
            this.HoraChegada,
            this.Data});
            this.dgvVisita.Location = new System.Drawing.Point(6, 122);
            this.dgvVisita.Name = "dgvVisita";
            this.dgvVisita.ReadOnly = true;
            this.dgvVisita.Size = new System.Drawing.Size(543, 216);
            this.dgvVisita.TabIndex = 10;
            // 
            // Visitante
            // 
            this.Visitante.DataPropertyName = "Visitante";
            this.Visitante.HeaderText = "Visitante";
            this.Visitante.Name = "Visitante";
            this.Visitante.ReadOnly = true;
            // 
            // Morador
            // 
            this.Morador.DataPropertyName = "Morador";
            this.Morador.HeaderText = "Morador";
            this.Morador.Name = "Morador";
            this.Morador.ReadOnly = true;
            // 
            // QtdAdultos
            // 
            this.QtdAdultos.DataPropertyName = "QtdAdultos";
            this.QtdAdultos.HeaderText = "Adultos";
            this.QtdAdultos.Name = "QtdAdultos";
            this.QtdAdultos.ReadOnly = true;
            // 
            // QtdCriancas
            // 
            this.QtdCriancas.DataPropertyName = "QtdCriancas";
            this.QtdCriancas.HeaderText = "Crianças";
            this.QtdCriancas.Name = "QtdCriancas";
            this.QtdCriancas.ReadOnly = true;
            // 
            // TpTransporte
            // 
            this.TpTransporte.DataPropertyName = "TpTransporte";
            this.TpTransporte.HeaderText = "Tipo de Transporte";
            this.TpTransporte.Name = "TpTransporte";
            this.TpTransporte.ReadOnly = true;
            // 
            // TpVisita
            // 
            this.TpVisita.DataPropertyName = "TpVisita";
            this.TpVisita.HeaderText = "Tipo de Visita";
            this.TpVisita.Name = "TpVisita";
            this.TpVisita.ReadOnly = true;
            // 
            // HoraChegada
            // 
            this.HoraChegada.DataPropertyName = "HoraChegada";
            this.HoraChegada.HeaderText = "Hora de Chegada";
            this.HoraChegada.Name = "HoraChegada";
            this.HoraChegada.ReadOnly = true;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(6, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 21);
            this.label5.TabIndex = 9;
            this.label5.Text = "Nome do Visitante:";
            // 
            // txtNomedoVisitante
            // 
            this.txtNomedoVisitante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomedoVisitante.Location = new System.Drawing.Point(262, 47);
            this.txtNomedoVisitante.Name = "txtNomedoVisitante";
            this.txtNomedoVisitante.Size = new System.Drawing.Size(287, 29);
            this.txtNomedoVisitante.TabIndex = 1;
            this.txtNomedoVisitante.TextChanged += new System.EventHandler(this.txtNomedoVisitante_TextChanged);
            // 
            // frmConsultarVisita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(582, 368);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConsultarVisita";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar Visita";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.DataGridView dgvVisita;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNomedoVisitante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Visitante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Morador;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdAdultos;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdCriancas;
        private System.Windows.Forms.DataGridViewTextBoxColumn TpTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn TpVisita;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraChegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
    }
}