﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void BtnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Login();
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void Login()
        {
            string usuario = txtUsuario.Text;
            string senha = txtSenha.Text;

            Business.LoginBusiness loginBusiness = new Business.LoginBusiness();
            Model.LoginModel loginModel = loginBusiness.Login(usuario, senha);

            if (loginModel != null)
            {
                Model.UsuarioLogado.ID = loginModel.ID;
                Model.UsuarioLogado.Nome = loginModel.Usuario;
                frmMenu tela = new frmMenu();
                tela.Show();

                this.Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.", "Visits Control", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            frmCadastrar tela = new frmCadastrar();
            tela.Show();
            this.Hide();
        }
    }
}
