﻿namespace BDTrabalho.Telas
{
    partial class frmDeletar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFechar = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNão = new System.Windows.Forms.Button();
            this.btnSim = new System.Windows.Forms.Button();
            this.lblTemcerteza = new System.Windows.Forms.Label();
            this.dgvVisita = new System.Windows.Forms.DataGridView();
            this.Visitante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Morador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdAdultos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdCriancas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TpTransporte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TpVisita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraChegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboVisitante = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisita)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Black;
            this.lblFechar.Location = new System.Drawing.Point(570, -2);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(19, 21);
            this.lblFechar.TabIndex = 3;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::BDTrabalho.Properties.Resources.background_image;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.btnNão);
            this.groupBox1.Controls.Add(this.btnSim);
            this.groupBox1.Controls.Add(this.lblTemcerteza);
            this.groupBox1.Controls.Add(this.dgvVisita);
            this.groupBox1.Controls.Add(this.cboVisitante);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(563, 394);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deletar";
            // 
            // btnNão
            // 
            this.btnNão.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnNão.Location = new System.Drawing.Point(471, 360);
            this.btnNão.Name = "btnNão";
            this.btnNão.Size = new System.Drawing.Size(82, 28);
            this.btnNão.TabIndex = 33;
            this.btnNão.Text = "Não";
            this.btnNão.UseVisualStyleBackColor = false;
            this.btnNão.Visible = false;
            this.btnNão.Click += new System.EventHandler(this.BtnNão_Click);
            // 
            // btnSim
            // 
            this.btnSim.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSim.Location = new System.Drawing.Point(349, 360);
            this.btnSim.Name = "btnSim";
            this.btnSim.Size = new System.Drawing.Size(82, 28);
            this.btnSim.TabIndex = 32;
            this.btnSim.Text = "Sim";
            this.btnSim.UseVisualStyleBackColor = false;
            this.btnSim.Visible = false;
            this.btnSim.Click += new System.EventHandler(this.BtnSim_Click);
            // 
            // lblTemcerteza
            // 
            this.lblTemcerteza.AutoSize = true;
            this.lblTemcerteza.BackColor = System.Drawing.Color.Transparent;
            this.lblTemcerteza.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemcerteza.Location = new System.Drawing.Point(391, 332);
            this.lblTemcerteza.Name = "lblTemcerteza";
            this.lblTemcerteza.Size = new System.Drawing.Size(123, 25);
            this.lblTemcerteza.TabIndex = 31;
            this.lblTemcerteza.Text = "Tem certeza?";
            this.lblTemcerteza.Visible = false;
            // 
            // dgvVisita
            // 
            this.dgvVisita.AllowUserToAddRows = false;
            this.dgvVisita.AllowUserToDeleteRows = false;
            this.dgvVisita.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvVisita.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisita.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Visitante,
            this.Morador,
            this.QtdAdultos,
            this.QtdCriancas,
            this.TpTransporte,
            this.TpVisita,
            this.HoraChegada,
            this.Data});
            this.dgvVisita.Location = new System.Drawing.Point(10, 68);
            this.dgvVisita.Name = "dgvVisita";
            this.dgvVisita.ReadOnly = true;
            this.dgvVisita.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVisita.Size = new System.Drawing.Size(543, 216);
            this.dgvVisita.TabIndex = 30;
            this.dgvVisita.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvVisita_CellClick);
            // 
            // Visitante
            // 
            this.Visitante.DataPropertyName = "Visitante";
            this.Visitante.HeaderText = "Visitante";
            this.Visitante.Name = "Visitante";
            this.Visitante.ReadOnly = true;
            // 
            // Morador
            // 
            this.Morador.DataPropertyName = "Morador";
            this.Morador.HeaderText = "Morador";
            this.Morador.Name = "Morador";
            this.Morador.ReadOnly = true;
            // 
            // QtdAdultos
            // 
            this.QtdAdultos.DataPropertyName = "QtdAdultos";
            this.QtdAdultos.HeaderText = "Adultos";
            this.QtdAdultos.Name = "QtdAdultos";
            this.QtdAdultos.ReadOnly = true;
            // 
            // QtdCriancas
            // 
            this.QtdCriancas.DataPropertyName = "QtdCriancas";
            this.QtdCriancas.HeaderText = "Crianças";
            this.QtdCriancas.Name = "QtdCriancas";
            this.QtdCriancas.ReadOnly = true;
            // 
            // TpTransporte
            // 
            this.TpTransporte.DataPropertyName = "TpTransporte";
            this.TpTransporte.HeaderText = "Tipo de Transporte";
            this.TpTransporte.Name = "TpTransporte";
            this.TpTransporte.ReadOnly = true;
            // 
            // TpVisita
            // 
            this.TpVisita.DataPropertyName = "TpVisita";
            this.TpVisita.HeaderText = "Tipo de Visita";
            this.TpVisita.Name = "TpVisita";
            this.TpVisita.ReadOnly = true;
            // 
            // HoraChegada
            // 
            this.HoraChegada.DataPropertyName = "HoraChegada";
            this.HoraChegada.HeaderText = "Hora de Chegada";
            this.HoraChegada.Name = "HoraChegada";
            this.HoraChegada.ReadOnly = true;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // cboVisitante
            // 
            this.cboVisitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVisitante.FormattingEnabled = true;
            this.cboVisitante.Location = new System.Drawing.Point(266, 33);
            this.cboVisitante.Name = "cboVisitante";
            this.cboVisitante.Size = new System.Drawing.Size(287, 29);
            this.cboVisitante.TabIndex = 29;
            this.cboVisitante.SelectedIndexChanged += new System.EventHandler(this.CboVisitante_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(6, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 21);
            this.label1.TabIndex = 28;
            this.label1.Text = "Visitante:";
            // 
            // frmDeletar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(587, 416);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmDeletar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Deletar";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvVisita;
        private System.Windows.Forms.DataGridViewTextBoxColumn Visitante;
        private System.Windows.Forms.DataGridViewTextBoxColumn Morador;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdAdultos;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdCriancas;
        private System.Windows.Forms.DataGridViewTextBoxColumn TpTransporte;
        private System.Windows.Forms.DataGridViewTextBoxColumn TpVisita;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraChegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.ComboBox cboVisitante;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSim;
        private System.Windows.Forms.Label lblTemcerteza;
        private System.Windows.Forms.Button btnNão;
    }
}