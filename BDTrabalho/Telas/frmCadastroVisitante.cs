﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmCadastroVisitante : Form
    {
        public frmCadastroVisitante()
        {
            InitializeComponent();
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Cadastrar();
        }

        private void Cadastrar()
        {
            try
            {
                Model.VisitanteModel visitante = new Model.VisitanteModel();
                visitante.Nome = txtNome.Text;
                visitante.NdeContato = txtNdeContato.Text;

                Business.VisitanteBusiness visitaBusiness = new Business.VisitanteBusiness();
                visitaBusiness.InserirVisitante(visitante);

                MessageBox.Show("Visitante inserido com sucesso.", "Visitante", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
