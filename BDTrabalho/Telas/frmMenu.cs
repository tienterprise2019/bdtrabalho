﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();

            if (Model.UsuarioLogado.ID == 1)
            {
                btnVisitante.Visible = true;
            }

            if (Model.UsuarioLogado.ID == 1)
            {
                btnMarcarVisita.Visible = true;
            }

            if (Model.UsuarioLogado.ID == 1)
            {
                btnMorador.Visible = true;
            }

            if (Model.UsuarioLogado.ID == 1)
            {
                btnAlterar.Visible = true;
            }

            if (Model.UsuarioLogado.ID == 1)
            {
                btnDeletar.Visible = true;
            }
        }

        private void BtnVisitante_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroVisitante tela = new frmCadastroVisitante();
            tela.Show();
        }

        private void BtnMarcarVisita_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMarcarVisita tela = new frmMarcarVisita();
            tela.Show();
        }

        private void BtnConsulta_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarVisita tela = new frmConsultarVisita();
            tela.Show();
        }

        private void BtnConsultarVisita_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarVisitante tela = new frmConsultarVisitante();
            tela.Show();
        }

        private void BtnMorador_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastroMorador tela = new frmCadastroMorador();
            tela.Show();
        }

        private void BtnAlterar_Click(object sender, EventArgs e)
        {

            this.Hide();
            frmAlterar tela = new frmAlterar();
            tela.Show();
        }

        private void BtnDeletar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmDeletar tela = new frmDeletar();
            tela.Show();
        }

        private void BtnConsultarMorador_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarMorador tela = new frmConsultarMorador();
            tela.Show();
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin tela = new frmLogin();
            tela.Show();
        }
    }
}
