﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmConsultarVisita : Form
    {
        public frmConsultarVisita()
        {
            InitializeComponent();
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void txtNomedoVisitante_TextChanged(object sender, EventArgs e)
        {
            this.Buscar();
        }

        private void dtpData_ValueChanged(object sender, EventArgs e)
        {
            this.Buscar();
        }

        private void Buscar()
        {
            try
            {
                string visitante = txtNomedoVisitante.Text;
                DateTime data = dtpData.Value.Date;

                Business.VisitaBusiness b = new Business.VisitaBusiness();
                List<Model.ConsultaVisitanteView> lista = b.ListarVisitas(visitante, data);

                dgvVisita.AutoGenerateColumns = false;
                dgvVisita.DataSource = lista;
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
