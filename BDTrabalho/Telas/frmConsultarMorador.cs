﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmConsultarMorador : Form
    {
        public frmConsultarMorador()
        {
            InitializeComponent();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            this.Consultar();
        }

        private void Consultar()
        {
            try
            {
                string nome = txtNome.Text;

                Business.MoradorBusiness moradorBusiness = new Business.MoradorBusiness();
                List<Model.MoradorModel> lista = moradorBusiness.FiltrarPorNome(nome);

                dgvMorador.AutoGenerateColumns = false;
                dgvMorador.DataSource = lista;
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch(Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void lblFecha_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }
    }
}
