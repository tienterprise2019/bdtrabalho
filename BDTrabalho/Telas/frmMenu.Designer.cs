﻿namespace BDTrabalho.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConsultarMorador = new System.Windows.Forms.Button();
            this.btnDeletar = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnMorador = new System.Windows.Forms.Button();
            this.btnConsultarVisita = new System.Windows.Forms.Button();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.btnMarcarVisita = new System.Windows.Forms.Button();
            this.btnVisitante = new System.Windows.Forms.Button();
            this.lblFechar = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::BDTrabalho.Properties.Resources.background_image;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.btnConsultarMorador);
            this.groupBox1.Controls.Add(this.btnDeletar);
            this.groupBox1.Controls.Add(this.btnAlterar);
            this.groupBox1.Controls.Add(this.btnMorador);
            this.groupBox1.Controls.Add(this.btnConsultarVisita);
            this.groupBox1.Controls.Add(this.btnConsulta);
            this.groupBox1.Controls.Add(this.btnMarcarVisita);
            this.groupBox1.Controls.Add(this.btnVisitante);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 152);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Menu";
            // 
            // btnConsultarMorador
            // 
            this.btnConsultarMorador.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnConsultarMorador.Location = new System.Drawing.Point(340, 96);
            this.btnConsultarMorador.Name = "btnConsultarMorador";
            this.btnConsultarMorador.Size = new System.Drawing.Size(86, 50);
            this.btnConsultarMorador.TabIndex = 10;
            this.btnConsultarMorador.Text = "Consultar Morador";
            this.btnConsultarMorador.UseVisualStyleBackColor = false;
            this.btnConsultarMorador.Click += new System.EventHandler(this.BtnConsultarMorador_Click);
            // 
            // btnDeletar
            // 
            this.btnDeletar.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnDeletar.Location = new System.Drawing.Point(233, 96);
            this.btnDeletar.Name = "btnDeletar";
            this.btnDeletar.Size = new System.Drawing.Size(86, 50);
            this.btnDeletar.TabIndex = 9;
            this.btnDeletar.Text = "Deletar Visita";
            this.btnDeletar.UseVisualStyleBackColor = false;
            this.btnDeletar.Visible = false;
            this.btnDeletar.Click += new System.EventHandler(this.BtnDeletar_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnAlterar.Location = new System.Drawing.Point(117, 96);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(86, 50);
            this.btnAlterar.TabIndex = 8;
            this.btnAlterar.Text = "Alterar Visita";
            this.btnAlterar.UseVisualStyleBackColor = false;
            this.btnAlterar.Visible = false;
            this.btnAlterar.Click += new System.EventHandler(this.BtnAlterar_Click);
            // 
            // btnMorador
            // 
            this.btnMorador.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnMorador.Location = new System.Drawing.Point(6, 96);
            this.btnMorador.Name = "btnMorador";
            this.btnMorador.Size = new System.Drawing.Size(86, 50);
            this.btnMorador.TabIndex = 7;
            this.btnMorador.Text = "Cadastrar Morador";
            this.btnMorador.UseVisualStyleBackColor = false;
            this.btnMorador.Visible = false;
            this.btnMorador.Click += new System.EventHandler(this.BtnMorador_Click);
            // 
            // btnConsultarVisita
            // 
            this.btnConsultarVisita.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnConsultarVisita.Location = new System.Drawing.Point(340, 28);
            this.btnConsultarVisita.Name = "btnConsultarVisita";
            this.btnConsultarVisita.Size = new System.Drawing.Size(86, 50);
            this.btnConsultarVisita.TabIndex = 6;
            this.btnConsultarVisita.Text = "Consultar Visitante";
            this.btnConsultarVisita.UseVisualStyleBackColor = false;
            this.btnConsultarVisita.Click += new System.EventHandler(this.BtnConsultarVisita_Click);
            // 
            // btnConsulta
            // 
            this.btnConsulta.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnConsulta.Location = new System.Drawing.Point(233, 28);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(86, 50);
            this.btnConsulta.TabIndex = 5;
            this.btnConsulta.Text = "Consultar Visita";
            this.btnConsulta.UseVisualStyleBackColor = false;
            this.btnConsulta.Click += new System.EventHandler(this.BtnConsulta_Click);
            // 
            // btnMarcarVisita
            // 
            this.btnMarcarVisita.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnMarcarVisita.Location = new System.Drawing.Point(117, 28);
            this.btnMarcarVisita.Name = "btnMarcarVisita";
            this.btnMarcarVisita.Size = new System.Drawing.Size(86, 50);
            this.btnMarcarVisita.TabIndex = 4;
            this.btnMarcarVisita.Text = "Marcar Visita";
            this.btnMarcarVisita.UseVisualStyleBackColor = false;
            this.btnMarcarVisita.Visible = false;
            this.btnMarcarVisita.Click += new System.EventHandler(this.BtnMarcarVisita_Click);
            // 
            // btnVisitante
            // 
            this.btnVisitante.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnVisitante.Location = new System.Drawing.Point(6, 28);
            this.btnVisitante.Name = "btnVisitante";
            this.btnVisitante.Size = new System.Drawing.Size(86, 50);
            this.btnVisitante.TabIndex = 3;
            this.btnVisitante.Text = "Cadastrar Visitante";
            this.btnVisitante.UseVisualStyleBackColor = false;
            this.btnVisitante.Visible = false;
            this.btnVisitante.Click += new System.EventHandler(this.BtnVisitante_Click);
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblFechar.Location = new System.Drawing.Point(440, -1);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(19, 21);
            this.lblFechar.TabIndex = 11;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.LblFechar_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(456, 172);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnConsultarMorador;
        private System.Windows.Forms.Button btnDeletar;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnMorador;
        private System.Windows.Forms.Button btnConsultarVisita;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.Button btnMarcarVisita;
        private System.Windows.Forms.Button btnVisitante;
        private System.Windows.Forms.Label lblFechar;
    }
}