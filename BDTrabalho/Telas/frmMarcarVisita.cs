﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmMarcarVisita : Form
    {
        public frmMarcarVisita()
        {
            InitializeComponent();

            this.CarregarVisitantes();
            this.CarregarMoradores();
        }

        private void CarregarVisitantes()
        {
            Business.VisitaBusiness marcarvisitabusiness = new Business.VisitaBusiness();
            List<Model.VisitanteModel> visitantes = marcarvisitabusiness.ListarVisitante();

            cboVisitante.DisplayMember = nameof(Model.VisitanteModel.Nome);
            cboVisitante.DataSource = visitantes;
        }
        private void CarregarMoradores()
        {
            Business.VisitaBusiness marcarvisitabusiness = new Business.VisitaBusiness();
            List<Model.MoradorModel> moradores = marcarvisitabusiness.ListarMoradores();

            cboMorador.DisplayMember = nameof(Model.MoradorModel.Nome);
            cboMorador.DataSource = moradores;
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Model.VisitanteModel visitante = cboVisitante.SelectedItem as Model.VisitanteModel;
                Model.MoradorModel morador = cboMorador.SelectedItem as Model.MoradorModel;

                Model.VisitaModel model = new Model.VisitaModel();
                model.IdVisitante = visitante.ID;
                model.IdMorador = morador.ID;
                model.qtddeadultos = Convert.ToInt32(nudQtddeAdultos.Value);
                model.qtddecriancas = Convert.ToInt32(nudQtddeCriancas.Value);
                model.tipodetransporte = cboTipodeTransporte.Text;
                model.conhecido = rdnConhecido.Checked;
                model.desconhecido = rdnDesconhecido.Checked;
                model.horadechegada = TimeSpan.Parse(txtHrdeChegada.Text);
                model.data = dtpData.Value;

                Business.VisitaBusiness marcarvisitaBusiness = new Business.VisitaBusiness();
                marcarvisitaBusiness.MarcarVisita(model);

                MessageBox.Show("Visita marcada com sucesso.", "Marcar Visita", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }
    }
}
