﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BDTrabalho.Telas
{
    public partial class frmDeletar : Form
    {
        public frmDeletar()
        {
            InitializeComponent();

            this.CarregarVisitantes();
        }

        private void CarregarVisitantes()
        {
            Business.VisitaBusiness marcarvisitabusiness = new Business.VisitaBusiness();
            List<Model.VisitanteModel> visitantes = marcarvisitabusiness.ListarVisitante();

            cboVisitante.DisplayMember = nameof(Model.VisitanteModel.Nome);
            cboVisitante.DataSource = visitantes;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void CboVisitante_SelectedIndexChanged(object sender, EventArgs e)
        {
            string visitante = cboVisitante.Text;

            Business.VisitaBusiness b = new Business.VisitaBusiness();
            List<Model.ConsultaVisitanteView> lista = b.ListarVisitasPorVisitante(visitante);

            dgvVisita.AutoGenerateColumns = false;
            dgvVisita.DataSource = lista;
        }

        private void BtnSim_Click(object sender, EventArgs e)
        {
            try
            {
                Model.ConsultaVisitanteView m = dgvVisita.CurrentRow.DataBoundItem as Model.ConsultaVisitanteView;

                int id = m.IdVisita;

                Business.VisitaBusiness visitaBusiness = new Business.VisitaBusiness();
                visitaBusiness.Deletar(id);

                MessageBox.Show("Visita removida com sucesso.", "Deletar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente mais tarde.");
            }
        }

        private void BtnNão_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMenu tela = new frmMenu();
            tela.Show();
        }

        private void DgvVisita_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Model.ConsultaVisitanteView m = dgvVisita.CurrentRow.DataBoundItem as Model.ConsultaVisitanteView;
            lblTemcerteza.Visible = true;
            btnSim.Visible = true;
            btnNão.Visible = true;
        }
    }
}
