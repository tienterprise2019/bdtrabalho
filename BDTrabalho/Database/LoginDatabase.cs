﻿using BD.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Database
{
    class LoginDatabase
    {
        public Model.LoginModel Login(string usuario, string senha)
        {
            string script = "select * from tb_login where nm_usuario = @usuario and ds_senha = @senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("usuario", usuario));
            parms.Add(new MySqlParameter("senha", senha));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.LoginModel model = null;

            if(reader.Read())
            {
                model = new Model.LoginModel();
                model.ID = Convert.ToInt32(reader["id_login"]);
                model.Usuario = Convert.ToString(reader["nm_usuario"]);
                model.Senha = Convert.ToString(reader["ds_senha"]);
            }
            reader.Close();

            return model;
        }

        public void Cadastrar(string usuario, string senha)
        {
            string script = @"insert into tb_login (nm_usuario, ds_senha)
                                              values (@nm_usuario, @ds_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario));
            parms.Add(new MySqlParameter("ds_senha", senha));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
