﻿using BD.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Database
{
    class VisitaDatabase
    {

        public void MarcarVisita(Model.VisitaModel model)
        {
            string script = @"insert into tb_visita (id_visitante, id_morador, qt_adultos, qt_criancas, tp_transporte, tp_visita, hr_chegada, dt_data)
                                             values (@id_visitante, @id_morador, @qt_adultos, @qt_criancas, @tp_transporte, @tp_visita, @hr_chegada, @dt_data)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_visitante", model.IdVisitante));
            parms.Add(new MySqlParameter("id_morador", model.IdMorador));
            parms.Add(new MySqlParameter("qt_adultos", model.qtddeadultos));
            parms.Add(new MySqlParameter("qt_criancas", model.qtddecriancas));
            parms.Add(new MySqlParameter("tp_transporte", model.tipodetransporte));
            parms.Add(new MySqlParameter("tp_visita", model.tipodevisita));
            parms.Add(new MySqlParameter("hr_chegada", model.horadechegada));
            parms.Add(new MySqlParameter("dt_data", model.data));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }


        public List<Model.ConsultaVisitanteView> ListarVisitas(string visitante, DateTime data)
        {
            string script = @"select tb_morador.id_morador, 
                                       tb_visitante.id_visitante, 
                                       tb_visita.id_visita,
                                       nm_morador,
	                                   nm_visitante,
	                                   qt_adultos,
	                                   qt_criancas,
                                       tp_transporte,
                                       tp_visita,
                                       hr_chegada,
                                       dt_data
                                  from tb_visita
                                  join tb_morador 
                                    on tb_visita.id_morador = tb_morador.id_morador
                                  join tb_visitante 
                                    on tb_visita.id_visitante = tb_visitante.id_visitante
                                  where nm_visitante like @visitante and dt_data = @data";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("visitante", "%"+visitante+"%"));
            parms.Add(new MySqlParameter("data", data));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ConsultaVisitanteView> lista = new List<Model.ConsultaVisitanteView>();

            while (reader.Read())
            {
                Model.ConsultaVisitanteView visita = new Model.ConsultaVisitanteView();
                visita.IdVisita = Convert.ToInt32(reader["id_visita"]);
                visita.IdMorador = Convert.ToInt32(reader["id_morador"]);
                visita.IdVisitante = Convert.ToInt32(reader["id_visitante"]);
                visita.Morador = Convert.ToString(reader["nm_morador"]);
                visita.Visitante = Convert.ToString(reader["nm_visitante"]);
                visita.QtdAdultos = Convert.ToInt32(reader["qt_adultos"]);
                visita.QtdCriancas = Convert.ToInt32(reader["qt_criancas"]);
                visita.TpTransporte = Convert.ToString(reader["tp_transporte"]);
                visita.TpVisita = Convert.ToString(reader["tp_visita"]);
                visita.HoraChegada = TimeSpan.Parse(reader["hr_chegada"].ToString());
                visita.Data = Convert.ToDateTime(reader["dt_data"]);

                lista.Add(visita);
            }
            reader.Close();

            return lista;
        }

        public List<Model.ConsultaVisitanteView> ListarVisitasPorVisitante(string visitante)
        {
            string script = @"select tb_morador.id_morador, 
                                       tb_visitante.id_visitante, 
                                       tb_visita.id_visita,
                                       nm_morador,
	                                   nm_visitante,
	                                   qt_adultos,
	                                   qt_criancas,
                                       tp_transporte,
                                       tp_visita,
                                       hr_chegada,
                                       dt_data
                                  from tb_visita
                                  join tb_morador 
                                    on tb_visita.id_morador = tb_morador.id_morador
                                  join tb_visitante 
                                    on tb_visita.id_visitante = tb_visitante.id_visitante
                                  where nm_visitante like @visitante";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("visitante", "%" + visitante + "%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ConsultaVisitanteView> lista = new List<Model.ConsultaVisitanteView>();

            while (reader.Read())
            {
                Model.ConsultaVisitanteView visita = new Model.ConsultaVisitanteView();
                visita.IdVisita = Convert.ToInt32(reader["id_visita"]);
                visita.IdMorador = Convert.ToInt32(reader["id_morador"]);
                visita.IdVisitante = Convert.ToInt32(reader["id_visitante"]);
                visita.Morador = Convert.ToString(reader["nm_morador"]);
                visita.Visitante = Convert.ToString(reader["nm_visitante"]);
                visita.QtdAdultos = Convert.ToInt32(reader["qt_adultos"]);
                visita.QtdCriancas = Convert.ToInt32(reader["qt_criancas"]);
                visita.TpTransporte = Convert.ToString(reader["tp_transporte"]);
                visita.TpVisita = Convert.ToString(reader["tp_visita"]);
                visita.HoraChegada = TimeSpan.Parse(reader["hr_chegada"].ToString());
                visita.Data = Convert.ToDateTime(reader["dt_data"]);

                lista.Add(visita);
            }
            reader.Close();

            return lista;
        }

        public void Alterar(Model.VisitaModel model)
        {
            string script = @"update tb_visita
                                set qt_adultos  = @qt_adultos,
                                   qt_criancas  = @qt_criancas,
                                 tp_transporte  = @tp_transporte,
	                                 tp_visita  = @tp_visita,
                                    hr_chegada  = @hr_chegada,
                                       dt_data  = @dt_data
                              where  id_visita  = @id_visita";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qt_adultos", model.qtddeadultos));
            parms.Add(new MySqlParameter("qt_criancas", model.qtddecriancas));
            parms.Add(new MySqlParameter("tp_transporte", model.tipodetransporte));
            parms.Add(new MySqlParameter("tp_visita", model.tipodevisita));
            parms.Add(new MySqlParameter("hr_chegada", model.horadechegada));
            parms.Add(new MySqlParameter("dt_data", model.data));
            parms.Add(new MySqlParameter("id_visita", model.Id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public void Deletar(int id)
        {
            string script = @"delete from tb_visita where id_visita  = @id_visita";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_visita", id));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
