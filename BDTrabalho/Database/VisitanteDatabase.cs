﻿using BD.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Database
{
    class VisitanteDatabase
    {
        public void InserirVisitante(Model.VisitanteModel visitante)
        {
            string script = @"insert into tb_visitante (nm_visitante, nm_contato)
                                              values (@nm_visitante, @nm_contato)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_visitante", visitante.Nome));
            parms.Add(new MySqlParameter("nm_contato", visitante.NdeContato));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public List<Model.VisitanteModel> FiltrarPorNome(string nome)
        {
            string script = "select * from tb_visitante where nm_visitante like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", "%"+nome+"%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.VisitanteModel> lista = new List<Model.VisitanteModel>();

            while (reader.Read())
            {
                Model.VisitanteModel visitante = new Model.VisitanteModel();
                visitante.ID = Convert.ToInt32(reader["id_visitante"]);
                visitante.Nome = Convert.ToString(reader["nm_visitante"]);
                visitante.NdeContato = Convert.ToString(reader["nm_contato"]);

                lista.Add(visitante);
            }
            reader.Close();

            return lista;
        }

        public List<Model.VisitanteModel> ListarVisitante()
        {
            string script = @"select *
                                from tb_visitante
                                order
                                by nm_visitante";

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<Model.VisitanteModel> lista = new List<Model.VisitanteModel>();

            while (reader.Read())
            {
                Model.VisitanteModel visitante = new Model.VisitanteModel();
                visitante.ID = Convert.ToInt32(reader["id_visitante"]);
                visitante.Nome = Convert.ToString(reader["nm_visitante"]);
                visitante.NdeContato = Convert.ToString(reader["nm_contato"]);

                lista.Add(visitante);
            }
            reader.Close();

            return lista;
        }
    }
}
