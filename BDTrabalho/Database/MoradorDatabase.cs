﻿using BD.Database;
using BDTrabalho.Database;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Database
{
    class MoradorDatabase
    {
        public void InserirMorador(Model.MoradorModel morador)
        {
            string script = @"insert into tb_morador (nm_morador, nm_contato, bc_bloco, nm_ap)
                                              values (@nm_morador, @nm_contato, @bc_bloco, @nm_ap)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_morador", morador.Nome));
            parms.Add(new MySqlParameter("nm_contato", morador.NdeContato));
            parms.Add(new MySqlParameter("bc_bloco", morador.Bloco));
            parms.Add(new MySqlParameter("nm_ap", morador.NdoAp));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }

        public List<Model.MoradorModel> FiltrarPorNome(string nome)
        {
            string script = "select * from tb_morador where nm_morador like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", "%"+nome+"%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.MoradorModel> lista = new List<Model.MoradorModel>();

            while (reader.Read())
            {
                Model.MoradorModel morador = new Model.MoradorModel();
                morador.ID = Convert.ToInt32(reader["id_morador"]);
                morador.Nome = Convert.ToString(reader["nm_morador"]);
                morador.NdeContato = Convert.ToString(reader["nm_contato"]);
                morador.Bloco = Convert.ToString(reader["bc_bloco"]);
                morador.NdoAp = Convert.ToInt32(reader["nm_ap"]);

                lista.Add(morador);
            }
            reader.Close();

            return lista;
        }

        public List<Model.MoradorModel> ListarMoradores()
        {
            string script = @"select *
                                from tb_morador
                                order
                                by nm_morador";

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<Model.MoradorModel> lista = new List<Model.MoradorModel>();

            while (reader.Read())
            {
                Model.MoradorModel moradores = new Model.MoradorModel();
                moradores.ID = Convert.ToInt32(reader["id_morador"]);
                moradores.Nome = Convert.ToString(reader["nm_morador"]);
                moradores.NdeContato = Convert.ToString(reader["nm_contato"]);
                moradores.Bloco = Convert.ToString(reader["bc_bloco"]);
                moradores.NdoAp = Convert.ToInt32(reader["nm_ap"]);

                lista.Add(moradores);
            }
            reader.Close();

            return lista;
        }
    }
}
