﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Model
{
    static class UsuarioLogado
    {
        public static int ID { get; set; }
        public static string Nome { get; set; }
    }
}
