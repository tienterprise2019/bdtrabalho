﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Model
{
    class ConsultaVisitanteView
    {
        public int IdVisita { get; set; }
        public int IdVisitante { get; set; }
        public int IdMorador { get; set; }


        public string Morador { get; set; }
        public string Visitante { get; set; }
        public int QtdAdultos { get; set; }
        public int QtdCriancas { get; set; }
        public string TpTransporte { get; set; }
        public string TpVisita { get; set; }
        public TimeSpan HoraChegada { get; set; }
        public DateTime Data { get; set; }

    }
}
