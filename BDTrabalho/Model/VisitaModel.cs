﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Model
{
    class VisitaModel
    {
        public int Id { get; set; }
        public int IdVisitante { get; set; }
        public int IdMorador { get; set; }
        public int qtddeadultos { get; set; }
        public int qtddecriancas { get; set; }
        public string tipodetransporte { get; set; }
        public string tipodevisita { get; set; }
        public bool conhecido { get; set; }
        public bool desconhecido { get; set; }
        public TimeSpan horadechegada { get; set; }
        public DateTime data { get; set; }
    }
}
