﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDTrabalho.Model
{
    class VisitanteModel
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string NdeContato { get; set; }
    }
}
